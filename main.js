postsDataGetter();

const landingPage = document.querySelector('.post-list');
const innerPage = document.querySelector('.post-block');
if (landingPage) {
  createPostsNav();
  createPosts();
}

if (innerPage) {
  createPage();
  creatPostComment();
}

async function postsDataGetter () {
  const pageParams = new URLSearchParams(window.location.search);
  const postPage = pageParams.get('page');

  const response = await fetch(`https://gorest.co.in/public-api/posts?page=${postPage == null ? 1 : postPage}`);
  const data = await response.json();
  return {
    posts: data.data,
    pagination: data.meta.pagination,
  }
}

async function createPostsNav () {
  let pagesArray = [];
  const pagination = await postsDataGetter();
  const back = document.querySelector('.page-link__back');
  const forward = document.querySelector('.page-item__forward');
  const navList = document.querySelector('.page-item__insert');
  const pageParams = new URLSearchParams(window.location.search);
  const postPage = pageParams.get('page');


  let postsNav = '';

  for (i=1; i<=pagination.pagination.pages; i++) {
    pagesArray.push(i);
  }
  /////////////////////////

  if (postPage == null || isNaN(postPage) || postPage <= 1) {
    postsNav += `
    <div class="page-item"> <a class="page-link test" href="index.html?page=${pagesArray[0]}">${pagesArray[0]}</a> </div>
    <div class="page-item"> <a class="page-link test" href="index.html?page=${pagesArray[0]+1}">${pagesArray[0]+1}</a> </div>
    <div class="page-item"> <a class="page-link test" href="index.html?page=${pagesArray[0]+2}">${pagesArray[0]+2}</a> </div>
    <div> . . . </div>
    <div class="page-item"> <a class="page-link test" href="index.html?page=${pagesArray.length}">${pagesArray.length}</a> </div>
   `;
  }
  else if (postPage == pagination.pagination.pages) {
    postsNav += `
    <div class="page-item"> <a class="page-link test" href="index.html?page=${pagesArray[0]}">${pagesArray[0]}</a> </div>
    <div> . . . </div>
    <div class="page-item"> <a class="page-link test" href="index.html?page=${parseInt(postPage)-1}">${parseInt(postPage)-1}</a> </div>
    <div class="page-item"> <a class="page-link test" href="index.html?page=${parseInt(postPage)}">${parseInt(postPage)}</a> </div>

    `;
  }
  else {
    postsNav += `
    <div class="page-item"> <a class="page-link test" href="index.html?page=${pagesArray[0]}">${pagesArray[0]}</a> </div>
    <div> . . . </div>
    <div class="page-item"> <a class="page-link test" href="index.html?page=${parseInt(postPage)-1}">${parseInt(postPage)-1}</a> </div>
    <div class="page-item"> <a class="page-link test" href="index.html?page=${parseInt(postPage)}">${parseInt(postPage)}</a> </div>
    <div class="page-item"> <a class="page-link test" href="index.html?page=${parseInt(postPage)+1}">${parseInt(postPage)+1}</a> </div>
    <div> . . . </div>
    <div class="page-item"> <a class="page-link test" href="index.html?page=${pagesArray.length}">${pagesArray.length}</a> </div>
    `;
  }

  navList.innerHTML = postsNav;
}

async function createPosts () {
  const posts = await postsDataGetter();
  const postsList = document.querySelector('.post-list')

  let postItem = '';

  for (i=0; i<posts.posts.length; i++) {
    postItem += `
    <div class="post-item card">
      <h5 class="post-item__title mb-1" >${posts.posts[i].title}</h5>
      <a class="post-list__link" href="index-post.html?id=${posts.posts[i].id}">
        Статья ${i+1} полностью
      </a>
    </div>
    `;
    postsList.innerHTML = postItem;
  }
}

async function createPage () {
  const page = document.querySelector('.post-block');
  let postContent = '';

  const pageParams = new URLSearchParams(location.search);
  const postId = pageParams.get('id');

  const response = await fetch(`https://gorest.co.in/public-api/posts/${postId}`);
  const data = await response.json();
  const postInfo = data.data;

  postContent = `
  <div class="card">
    <div class="card-body">
      <h1 class="card-title">${postInfo.title}</h1>
      <p class="card-text">${postInfo.body}</p>
    </div>
  </div>
  `;
  page.innerHTML = postContent;
}

async function creatPostComment () {
  const commentBlock = document.querySelector('.comments-block');
  let commentContent = '';

  const pageParams = new URLSearchParams(location.search);
  const postId = pageParams.get('id');

  const response = await fetch(`https://gorest.co.in/public-api/comments?post_id=${postId}`);
  const data = await response.json();
  const comment = data.data;

  comment.map(el => {
    commentContent = `
    <div class="card">
    <div class="card-body">
      <h1 class="card-title">${el.name}</h1>
      <p class="card-text">${el.body}</p>
      <p class="card-text">${el.email}</p>
    </div>
  </div>
    `;
    commentBlock.innerHTML = commentContent;
  })

}
